Winlink Templates
=================

This is the repository for Winlink form templates used by the
Issaquah Communication Support Team, the ARES/RACES team for the City
of Issaquah, WA.

The forms included here are:

- Winlink Net Checkin
  - Weekly nets
  - Weather nets
  - Training and Drill nets
  - Deployments

For more information on Winlink forms and how to use them, it is suggested
that you watch [this video](https://www.youtube.com/watch?v=SWuBmkCK_CQ)
from K4REF that covers the basics of using forms within Winlink.

Installing Winlink Forms
------------------------

There are three files that make up a Winlink form: two HTML files and a
text file. All three files will have the same base filename.

All that needs to be done is to copy the three files to the `Templates`
directory in your Winlink Express directory. If you have an older
installation, the directory name may be the older `RMS Express` instead
of `Winlink Express`.

Once you have located your installation of `Winlink Express`, you need to
decide how to make the form available. The form can be copied to
`Global Folders\Templates` to make it available to all that users that
use `Winlink Express`.

Alternatively, the form can be copied to a call sign specific directory
so that the forms are available just to that specific user. At the top
level of the `Winlink Express` directory there should be a directory
named after the call sign of the user to make the form available for.
If it does not exist, the `Templates` sub-directory may need to be
created in the call sign specific directory.

The general recommendation is to install the form into the `Global Folders`
directory.

After installing the forms it is important to restart `Winlink Express`
so that the form is properly imported into `Winlink Express`.

Reportings Errors and Requesting Features
-----------------------------------------

In the event you find an error with any of the forms in this repository,
please consider [creating an issue](https://gitlab.com/k7isq-code/winlink-templates/-/issues/new?issuable_template=Bug)
to describe the error and any examples that you can provide.

Just as important, if you have an idea or want to request a new feature,
please consider [submitting a feature request](https://gitlab.com/k7isq-code/winlink-templates/-/issues/new?issuable_template=Feature%20Request)
to start the discussion of flushing out the concept for the feature.
