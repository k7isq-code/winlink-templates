# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 1.0.1 (2022-03-22)


### Bug Fixes

* added debug CI job to troubleshoot additional jobs when release called ([d910baf](https://gitlab.com/k7isq-code/winlink-templates/-/commits/d910bafc421677a5f2bac620c115ea8a5dfd7144))
* Better compare links in CHANGELOG ([e15fb95](https://gitlab.com/k7isq-code/winlink-templates/-/commits/e15fb95eb7d598236f88061af43ca5eface92914))


### Changed

* Added more variations for conventional commits ([519df0e](https://gitlab.com/k7isq-code/winlink-templates/-/commits/519df0e441dd18ac9fac927c6d01bbe1a4f2a9d8))
* Restructure project to support multiple forms ([4cc8958](https://gitlab.com/k7isq-code/winlink-templates/-/commits/4cc8958f7e0dfee2c287347e095d4f5a420635d5))

## 1.0.0 (2022-03-06)

### Bug Fixes

* configuring git prior to release ([9e6edd3](https://wt0f.gitlab.com/k7isq-code/winlink-templates/-/commits/9e6edd31b1fc067c2ce3873a9f7203a0fbaf11ab))
* Corrected install of standard-version ([46f1558](https://wt0f.gitlab.com/k7isq-code/winlink-templates/-/commits/46f15581e0d5c6a505188b6e602e1aad2b011b71))
* Quoted curl command in release script ([876b5b2](https://wt0f.gitlab.com/k7isq-code/winlink-templates/-/commits/876b5b24dd378fe7223f659fc36fc00f200483f1))
* Release reference ([cefc072](https://wt0f.gitlab.com/k7isq-code/winlink-templates/-/commits/cefc07205fdddf28efac9ba936291fcfcc027bd1))


### Added

* Support for generating CHANGELOG ([ed14a57](https://wt0f.gitlab.com/k7isq-code/winlink-templates/-/commits/ed14a57f2f751f8df7b41091e2022c314cc2f5e7))


### Changed

* Refined release process ([5b8912a](https://wt0f.gitlab.com/k7isq-code/winlink-templates/-/commits/5b8912a70ada44f609826d4a6e3f6cac45e295d4))
* Removed date field and made automatic ([38fa030](https://wt0f.gitlab.com/k7isq-code/winlink-templates/-/commits/38fa03057a325885f437c3b6b1ab3dade4f8113a))
