const path = require('path');
const puppeteer = require('puppeteer');

const initialFilePath = path.join(__dirname, '..', 'winlink_checkin', 'ICST_Winlink_Checkin_Initial.html');
const url = `file://${initialFilePath}`;

const sections = ['Optional', 'Position', 'Deployment', 'Weather', 'Traffic'];

test('Confirm all sections hidden at page load', async () => {
  const browser = await puppeteer.launch();
  try {
    const page = await browser.newPage();

    await page.goto(url);

    await confirmSectionsVisibility(page);
  } finally {
    await browser.close();
  }
}, 120000);

test('Net NetType Shows Proper Sections', async () => {
  const browser = await puppeteer.launch();
  try {
    const page = await browser.newPage();

    await page.goto(url);

    await confirmSectionsVisibility(page);

    await setNetType(page, 'Net');

    await confirmSectionsVisibility(page, ['Optional']);
  } finally {
    await browser.close();
  }
}, 120000);

test('Drill NetType Shows Proper Sections', async () => {
  const browser = await puppeteer.launch();
  try {
    const page = await browser.newPage();

    await page.goto(url);

    await confirmSectionsVisibility(page);

    await setNetType(page, 'Drill');

    await confirmSectionsVisibility(page, ['Deployment', 'Traffic']);
  } finally {
    await browser.close();
  }
}, 120000);

async function setNetType(page, netType) {
  let netTypeSelector = await page.$(`#NetType`);
  await netTypeSelector.evaluate((el, typeVal) => {
    el.value = typeVal;
    el.onchange();
  }, netType);
}

async function confirmSectionsVisibility(page, shouldBeVisible = []) {
  for (const section of sections) {
    if (shouldBeVisible.includes(section)) {
      await expectSectionIsVisible(page, section);
    } else {
      await expectSectionIsHidden(page, section);
    }
  }
}

async function expectSectionIsVisible(page, id) {
  let section = await page.$(`#${id}`);
  let displayStyle = await section.evaluate((el) => el.style.display);
  expect(displayStyle).toEqual('block');
}

async function expectSectionIsHidden(page, id) {
  let section = await page.$(`#${id}`);
  let displayStyle = await section.evaluate((el) => el.style.display);
  expect(displayStyle).toEqual('none');
}
